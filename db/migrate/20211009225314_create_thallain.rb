class CreateThallain < ActiveRecord::Migration[6.1]
  def change
    create_table :thallain do |t|
      t.references :thallain_kith, null: false, foreign_key: true
      t.references :seeming, null: false, foreign_key: true
      t.references :court, null: false, foreign_key: true
      t.references :unseelie_legacy, null: false, foreign_key: true
      t.references :gender, null: false, foreign_key: true

      t.timestamps
    end
  end
end
