require 'randomize'

class Gender < ApplicationRecord
  def self.random
    Randomize::random_attribute(self)
  end
end
