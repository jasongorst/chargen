module.exports = {
  content: [
    './public/*.html',
    './app/assets/stylesheets/**/*.css',
    './app/helpers/**/*.rb',
    './app/javascript/**/*.js',
    './app/views/**/*.{erb,haml,html,slim}'
  ],
  plugins: [
    require("daisyui"),
    require("@tailwindcss/forms")({
      strategy: 'class'
    })
  ],
  daisyui: {
    themes: ["fantasy", "halloween"],
    darkTheme: "halloween",
    logs: false
  }
}
