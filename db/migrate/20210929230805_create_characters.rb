class CreateCharacters < ActiveRecord::Migration[6.1]
  def change
    create_table :characters do |t|
      t.references :kith, null: false, foreign_key: true
      t.references :seeming, null: false, foreign_key: true
      t.references :animal_seeming, null: false, foreign_key: true
      t.references :house, null: false, foreign_key: true
      t.references :bannerhouse, null: false, foreign_key: true
      t.references :seelie_legacy, null: false, foreign_key: true
      t.references :unseelie_legacy, null: false, foreign_key: true

      t.timestamps
    end
  end
end
