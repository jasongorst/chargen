class Kithain < Character
  attribute :kith
  attribute :animal_seeming
  attribute :phylum
  attribute :house
  attribute :bannerhouse
  attribute :seelie_legacy

  validates :kith, kind_of: true
  validates :animal_seeming, kind_of: true, allow_nil: true
  validates :phylum, kind_of: true, allow_nil: true
  validates :house, kind_of: true
  validates :bannerhouse, kind_of: true, allow_nil: true
  validates :seelie_legacy, kind_of: true
  validates_with KithainValidator

  # attributes names in order
  def self.attribute_names
    %w[kith seeming animal_seeming phylum house court bannerhouse seelie_legacy unseelie_legacy gender]
  end
end
