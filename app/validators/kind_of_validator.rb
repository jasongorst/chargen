class KindOfValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add(attribute, proc { "must be a #{attribute.classifize}" }) unless value.kind_of?(attribute.classifize)
  end
end
