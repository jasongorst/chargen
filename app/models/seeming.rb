require 'randomize'

class Seeming < ApplicationRecord
  def self.random
    Randomize::random_attribute(self)
  end
end
