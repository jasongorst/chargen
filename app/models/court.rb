require 'randomize'

class Court < ApplicationRecord
  def self.random(seelie = 50, unseelie = 50)
    rand(1..(seelie + unseelie)) <= seelie ? find_by_name('Seelie') : find_by_name('Unseelie')
  end
end
