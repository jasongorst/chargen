class CreateUnseelieLegacies < ActiveRecord::Migration[6.1]
  def change
    create_table :unseelie_legacies do |t|
      t.string :name
      t.references :rarity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
