class CharacterController < ApplicationController
  before_action :set_type
  before_action :new_character, only: %i[ index new ]

  # GET /character
  def index
    @type.attribute_names.each do |attribute|
      value = if params.dig(attribute).present?
                if params.dig(attribute) == 'none'
                  nil
                else
                  # Attribute.find(params.dig(:character, :attribute))
                  attribute.classify.constantize.find(params.dig(attribute))
                end
              else
                # random_attribute
                random_attribute(attribute)
              end

      # @character.attribute = value
      @character.send("#{attribute}=", value)
    end

    render 'character/index'
  end

  # GET /character/new
  def new
    render 'character/new'
  end

  # POST /character/regenerate
  def regenerate
    # handle locked attributes
    @query = {}

    # add value of locked attributes to query
    @type.attribute_names.each do |attribute|
      if params.dig("#{attribute}_lock").present?
        # @query[:attribute] = Attribute.find_by_name(params[:character][:attribute])
        @query[attribute.to_sym] = attribute.classify.constantize.find_by_name(params.dig(attribute))
      end
    end

    filter_query!

    # redirect to index path (with query)
    redirect_to URI::HTTP.build(path: send("#{@type.to_s.underscore}_path"), query: @query.to_query).request_uri
  end

  private

  def set_type
    # infer from class
    /(?<model>\w+?)Controller/ =~ self.class.to_s
    @type = model.constantize
  end

  def new_character
    @character = @type.new
  end

  def random_attribute(attribute)
    attribute.classify.constantize.random
  end

  def filter_query!; end
end
