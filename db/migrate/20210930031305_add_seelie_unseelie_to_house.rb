class AddSeelieUnseelieToHouse < ActiveRecord::Migration[6.1]
  def change
    add_column :houses, :seelie, :integer
    add_column :houses, :unseelie, :integer
  end
end
