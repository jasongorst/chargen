class String
  def classifize
    self.classify.constantize
  end
end

class Symbol
  def classifize
    self.to_s.classifize
  end
end
