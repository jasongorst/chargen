require 'randomize'

class SeelieLegacy < ApplicationRecord
  def self.random
    Randomize::random_attribute(self)
  end
end
