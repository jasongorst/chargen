require 'randomize'

class UnseelieLegacy < ApplicationRecord
  def self.random
    Randomize::random_attribute(self)
  end
end
