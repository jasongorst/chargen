class KithainController < CharacterController

  private

  def random_attribute(attribute)
    case attribute
    when 'animal_seeming'
      AnimalSeeming.random if @character.kith.name == 'Pooka'
    when 'phylum'
      Phylum.random if @character.kith.name == 'Inanimae'
    when 'house'
      House.random if @character.kith.name.start_with?('Sidhe') || Rarity.random.name == 'Rare'
    when 'court'
      unless @character.kith.name == 'Inanimae'
        if @character.house
          Court.random(@character.house.seelie, @character.house.unseelie)
        else
          Court.random
        end
      end
    when 'bannerhouse'
      if @character.kith.name.start_with?('Sidhe') && Rarity.random.name == 'Rare'
        case @character.house.name
        when 'House Liam'
          Bannerhouse.find_by_name('Truthbranch')
        when 'House Ailil'
          Bannerhouse.find_by_name('Cattecomb')
        else
          # no bannerhouse
          nil
        end
      end
    else
      super(attribute)
    end
  end

  def filter_query!
    # delete disallowed locked attributes from query hash
    @query.delete(:animal_seeming) if locked_value_of(:kith) != 'Pooka'
    @query.delete(:phylum) if locked_value_of(:kith) != 'Inanimae'
    @query.delete(:court) if locked_value_of(:kith) == 'Inanimae'

    @query.delete(:bannerhouse) if !(locked_value_of(:kith)&.start_with?('Sidhe')) ||
      (locked_value_of(:house) == 'House Liam' && locked_value_of(:bannerhouse) != 'Truthbranch') ||
      (locked_value_of(:house) == 'House Ailil' && locked_value_of(:bannerhouse) != 'Cattecomb')
  end

  def locked_value_of(attribute)
    @query.dig(attribute.to_sym)&.name
  end
end
