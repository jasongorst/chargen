require 'ranged_hash'

class Rarity < ApplicationRecord
  has_many :kiths
  has_many :animal_seemings
  has_many :houses
  has_many :bannerhouses
  has_many :thallain_kiths

  mattr_reader :index, :max

  # initialize rarity index
  @@index = RangedHash.new
  min = 1
  max = 0
  all.each do |r|
    max += r.value
    @@index[min..max] = r
    min = max + 1
  end

  # initialize total range
  @@max = max

  def self.random
    @@index[rand(1..@@max)]
  end
end
