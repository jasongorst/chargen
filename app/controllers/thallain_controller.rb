class ThallainController < CharacterController

  private

  def random_attribute(attribute)
    if attribute == 'court'
      # court is always Unseelie
      Court.find_by_name('Unseelie')
    else
      super(attribute)
    end
  end
end
