class ModifyCharactersToAllowNullsForAnimalSeemingAndHouseAndBannerhouse < ActiveRecord::Migration[6.1]
  def change
    change_column_null :characters, :animal_seeming_id, true
    change_column_null :characters, :house_id, true
    change_column_null :characters, :bannerhouse_id, true
  end
end
