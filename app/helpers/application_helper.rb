module ApplicationHelper
  module FormHelpers
    extend ActionView::Helpers::FormOptionsHelper

    def model_options(model)
      options_from_collection_for_select(model.all, :id, :name)
    end

    def model_options_with_rarity(model)
      option_groups_from_collection_for_select(Rarity.all, model, :name, :id, :name)
    end
  end
end
