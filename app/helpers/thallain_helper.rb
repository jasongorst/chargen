module ThallainHelper
  extend ActionView::Helpers::FormOptionsHelper
  extend ApplicationHelper::FormHelpers

  RANDOM_OPTION = options_for_select([['Random', '']])
  RANDOM_AND_NONE_OPTIONS = options_for_select([['Random', ''], ['None', 'none']])

  THALLAIN_ATTRIBUTES = {
    thallain_kith: {
      options: RANDOM_OPTION + model_options_with_rarity(:thallain_kiths),
      hidden?: false
    },
    seeming: {
      options: RANDOM_OPTION + model_options(Seeming),
      hidden?: false
    },
    court: {
      options: RANDOM_OPTION + model_options(Court),
      hidden?: false
    },
    unseelie_legacy: {
      options: RANDOM_OPTION + model_options(UnseelieLegacy),
      hidden: false
    },
    gender: {
      options: RANDOM_OPTION + model_options(Gender),
      hidden: false
    }
  }

  def thallain_attributes
    Thallain.attribute_names.map(&:to_sym)
  end

  def thallain_attributes_with_properties
    THALLAIN_ATTRIBUTES
  end
end
