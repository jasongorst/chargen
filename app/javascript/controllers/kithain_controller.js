import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [
      "animalSeeming",
      "phylum",
      "court",
      "bannerhouse",
      "kithSelect",
      "animalSeemingSelect",
      "phylumSelect",
      "houseSelect",
      "courtSelect",
      "bannerhouseSelect"
  ]

  update() {
    const kith = this.getSelectedOptionText(this.kithSelectTarget)
    const house = this.getSelectedOptionText(this.houseSelectTarget)
    const bannerhouse = this.getSelectedOptionText(this.bannerhouseSelectTarget)

    if (kith === "Pooka") {
      this.animalSeemingTarget.hidden = false
    } else {
      this.animalSeemingTarget.hidden = true
      this.setSelectToFirstOption(this.animalSeemingSelectTarget)
    }

    if (kith === "Inanimae") {
      this.courtTarget.hidden = true
      this.setSelectToFirstOption(this.courtSelectTarget)
      this.phylumTarget.hidden = false
    } else {
      this.courtTarget.hidden = false
      this.phylumTarget.hidden = true
      this.setSelectToFirstOption(this.phylumSelectTarget)
    }

    if (kith.startsWith("Sidhe") && (house === "House Liam" || house === "House Ailil")) {
      this.bannerhouseTarget.hidden = false
      if (house === "House Liam") {
        if (bannerhouse === "Cattecomb") {
          this.setSelectToFirstOption(this.bannerhouseSelectTarget)
        }
        this.setDisabledOfOptionByText(this.bannerhouseSelectTarget, "Truthbranch", false)
        this.setDisabledOfOptionByText(this.bannerhouseSelectTarget, "Cattecomb", true)
      } else {
        if (bannerhouse === "Truthbranch") {
          this.setSelectToFirstOption(this.bannerhouseSelectTarget)
        }
        this.setDisabledOfOptionByText(this.bannerhouseSelectTarget, "Truthbranch", true)
        this.setDisabledOfOptionByText(this.bannerhouseSelectTarget, "Cattecomb", false)
      }
    } else {
      this.bannerhouseTarget.hidden = true
      this.setSelectToFirstOption(this.bannerhouseSelectTarget)
    }
  }

  getSelectedOptionText(target) {
    return target.selectedOptions.item(0).text
  }

  setSelectToFirstOption(target) {
    target.selectedIndex = 0
  }

  setDisabledOfOptionByText(target, value, disabled) {
    for (let option of target.options) {
      if (option.text === value) {
        option.disabled = disabled
      }
    }
  }
}
