class DropThallain < ActiveRecord::Migration[7.0]
  def up
    drop_table :thallain
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
