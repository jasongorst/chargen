class Thallain < Character
  attribute :thallain_kith
  validates :thallain_kith, kind_of: true

  # attributes names in order
  def self.attribute_names
    %w[thallain_kith seeming court unseelie_legacy gender]
  end
end
