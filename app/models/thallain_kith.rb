require 'randomize'

class ThallainKith < ApplicationRecord
  belongs_to :rarity

  def self.random
    Randomize::random_attribute_with_rarity(self)
  end
end
