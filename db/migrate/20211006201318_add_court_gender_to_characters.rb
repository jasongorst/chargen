class AddCourtGenderToCharacters < ActiveRecord::Migration[6.1]
  def change
    add_reference :characters, :court, null: true, foreign_key: true
    add_reference :characters, :gender, null: false, foreign_key: true
  end
end
