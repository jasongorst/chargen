# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rarity.create [{ name: 'Common', value: 60 },
               { name: 'Uncommon', value: 30 },
               { name: 'Rare', value: 10 }]

COMMON = Rarity.find_by_name('Common')
UNCOMMON = Rarity.find_by_name('Uncommon')
RARE = Rarity.find_by_name('Rare')

Kith.create [{ name: 'Boggan', rarity: COMMON },
             { name: 'Clurichaun', rarity: COMMON },
             { name: 'Eshu', rarity: COMMON },
             { name: 'Nocker', rarity: COMMON },
             { name: 'Piskie', rarity: COMMON },
             { name: 'Pooka', rarity: COMMON },
             { name: 'Redcap', rarity: COMMON },
             { name: 'River Hag', rarity: COMMON },
             { name: 'Satyr', rarity: COMMON },
             { name: 'Sidhe-Arcadian', rarity: COMMON },
             { name: 'Sidhe-Autumn', rarity: COMMON },
             { name: 'Sluagh', rarity: COMMON },
             { name: 'Swan Maiden', rarity: COMMON },
             { name: 'Troll', rarity: COMMON }]

Kith.create [{ name: 'Wichtel', rarity: UNCOMMON },
             { name: 'Wolpertinger', rarity: UNCOMMON },
             { name: 'Oba', rarity: UNCOMMON },
             { name: 'Selkie', rarity: UNCOMMON },
             { name: 'Morganed', rarity: UNCOMMON }]

Kith.create [{ name: 'Ghille Dhu', rarity: RARE },
             { name: 'Korred', rarity: RARE },
             { name: 'Centaur', rarity: RARE },
             { name: 'Rusalka', rarity: RARE },
             { name: 'Merfolk', rarity: RARE },
             { name: 'Inanimae', rarity: RARE }]

Seeming.create [{ name: 'Childing' },
                { name: 'Wilder' },
                { name: 'Grump' }]

AnimalSeeming.create [{ name: 'Cat', rarity: COMMON },
                      { name: 'Dog', rarity: COMMON },
                      { name: 'Rodent', rarity: COMMON },
                      { name: 'Bird', rarity: COMMON }]

AnimalSeeming.create [{ name: 'Insect', rarity: UNCOMMON },
                      { name: 'Fish', rarity: UNCOMMON },
                      { name: 'Lizard/Reptile', rarity: UNCOMMON }]

AnimalSeeming.create [{ name: 'Elephant', rarity: RARE },
                      { name: 'Exotic', rarity: RARE }]

Phylum.create [{ name: 'Glome' },
               { name: 'Kubera' },
               { name: 'Mannikin' },
               { name: 'Ondine' },
               { name: 'Paroseme' },
               { name: 'Solimond' }]

House.create [{ name: 'House Dougal', rarity: COMMON, seelie: 80, unseelie: 20 },
              { name: 'House Eiluned', rarity: COMMON, seelie: 80, unseelie: 20 },
              { name: 'House Fiona', rarity: COMMON, seelie: 80, unseelie: 20 },
              { name: 'House Gwydion', rarity: COMMON, seelie: 90, unseelie: 10 },
              { name: 'House Liam', rarity: COMMON, seelie: 90, unseelie: 10 },
              { name: 'House Aesin', rarity: COMMON, seelie: 10, unseelie: 90 },
              { name: 'House Ailil', rarity: COMMON, seelie: 20, unseelie: 80 },
              { name: 'House Daireann', rarity: COMMON, seelie: 20, unseelie: 80 },
              { name: 'House Leanhaun', rarity: COMMON, seelie: 5, unseelie: 95 },
              { name: 'House Varich', rarity: COMMON, seelie: 15, unseelie: 85 },
              { name: 'House Scathach', rarity: COMMON, seelie: 50, unseelie: 50 }]

House.create [{ name: 'House Beaumayn', rarity: UNCOMMON, seelie: 90, unseelie: 10 },
              { name: 'House Balor', rarity: UNCOMMON, seelie: 5, unseelie: 95 }]

House.create [{ name: 'House Danaan', rarity: RARE, seelie: 50, unseelie: 50 }]

Bannerhouse.create [{ name: 'Truthbranch', rarity: RARE },
                    { name: 'Cattecomb', rarity: RARE }]

SeelieLegacy.create [{ name: 'Bumpkin' },
                     { name: 'Courtier' },
                     { name: 'Crafter' },
                     { name: 'Dandy' },
                     { name: 'Hermit' },
                     { name: 'Orchid' },
                     { name: 'Paladin' },
                     { name: 'Panderer' },
                     { name: 'Regent' },
                     { name: 'Sage' },
                     { name: 'Saint' },
                     { name: 'Squire' },
                     { name: 'Troubadour' },
                     { name: 'Wayfarer' }]

UnseelieLegacy.create [{ name: 'Beast' },
                       { name: 'Fatalist' },
                       { name: 'Fool' },
                       { name: 'Grotesque' },
                       { name: 'Knave' },
                       { name: 'Outlaw' },
                       { name: 'Pandora' },
                       { name: 'Peacock' },
                       { name: 'Rake' },
                       { name: 'Riddler' },
                       { name: 'Ringleader' },
                       { name: 'Rogue' },
                       { name: 'Savage' },
                       { name: 'Wretch' }]

Court.create [{ name: 'Seelie' },
              { name: 'Unseelie' }]

Gender.create [{ name: 'Male' },
               { name: 'Female' }]

ThallainKith.create [{ name: 'Aithu', rarity: COMMON },
                     { name: 'Beastie', rarity: COMMON },
                     { name: 'Boggart', rarity: COMMON },
                     { name: 'Bogie', rarity: COMMON },
                     { name: 'Goblin', rarity: COMMON },
                     { name: 'Ogre', rarity: COMMON },
                     { name: 'Spriggan', rarity: COMMON },
                     { name: 'Bodach', rarity: COMMON },
                     { name: 'Ghast', rarity: COMMON },
                     { name: 'Nastie', rarity: COMMON },
                     { name: 'Sevartal', rarity: COMMON }]

ThallainKith.create [{ name: 'Kelpie', rarity: UNCOMMON },
                     { name: 'Night Hag', rarity: UNCOMMON },
                     { name: 'Weeping Wight', rarity: UNCOMMON }]

ThallainKith.create [{ name: 'Lurk', rarity: RARE },
                     { name: 'Huaka’i Po', rarity: RARE },
                     { name: 'Mandragora', rarity: RARE },
                     { name: 'Skinwalker', rarity: RARE },
                     { name: 'Murdhuacha', rarity: RARE }]

User.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
