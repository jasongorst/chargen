require 'randomize'

class Phylum < ApplicationRecord
  def self.random
    Randomize::random_attribute(self)
  end
end
