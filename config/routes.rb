Rails.application.routes.draw do
  resources :passwords, controller: "clearance/passwords", only: [:create, :new]
  resource :session, controller: "clearance/sessions", only: [:create]

  resources :users, controller: "clearance/users", only: [:create] do
    resource :password,
             controller: "clearance/passwords",
             only: [:edit, :update]
  end

  get "/sign_in" => "clearance/sessions#new", as: "sign_in"
  delete "/sign_out" => "clearance/sessions#destroy", as: "sign_out"
  get "/sign_up" => "clearance/users#new", as: "sign_up"

  namespace :admin do
    resources :rarities
    resources :kiths
    resources :seemings
    resources :animal_seemings
    resources :phyla
    resources :houses
    resources :bannerhouses
    resources :courts
    resources :seelie_legacies
    resources :unseelie_legacies
    resources :genders
    resources :thallain_kiths

    root to: "rarities#index"
  end

  get 'kithain', to: 'kithain#index', as: 'kithain'
  get 'kithain/new', to: 'kithain#new', as: 'new_kithain'
  post 'kithain/regenerate', to: 'kithain#regenerate', as: 'regenerate_kithain'

  get 'thallain', to: 'thallain#index', as: 'thallain'
  get 'thallain/new', to: 'thallain#new', as: 'new_thallain'
  post 'thallain/regenerate', to: 'thallain#regenerate', as: 'regenerate_thallain'

  root 'kithain#index'
end
