class RemoveRarityIdFromUnseelieLegacies < ActiveRecord::Migration[6.1]
  def change
    remove_column :unseelie_legacies, :rarity_id
  end
end
