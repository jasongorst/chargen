class CreateAnimalSeemings < ActiveRecord::Migration[6.1]
  def change
    create_table :animal_seemings do |t|
      t.string :name
      t.references :rarity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
