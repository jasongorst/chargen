class RemoveRarityIdFromSeelieLegacies < ActiveRecord::Migration[6.1]
  def change
    remove_column :seelie_legacies, :rarity_id
  end
end
