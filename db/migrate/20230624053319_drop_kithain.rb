class DropKithain < ActiveRecord::Migration[7.0]
  def up
    drop_table :kithain
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
