class AddPhylumToCharacters < ActiveRecord::Migration[6.1]
  def change
    add_reference :characters, :phylum, null: true
  end
end
