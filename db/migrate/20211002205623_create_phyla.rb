class CreatePhyla < ActiveRecord::Migration[6.1]
  def change
    create_table :phyla do |t|
      t.string :name

      t.timestamps
    end
  end
end
