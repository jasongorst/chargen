require 'randomize'

class AnimalSeeming < ApplicationRecord
  belongs_to :rarity

  def self.random
    Randomize::random_attribute_with_rarity(self)
  end
end
