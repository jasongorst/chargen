// Entry point for the build script in your package.json

import mrujs from "mrujs"
import {dom, library} from "@fortawesome/fontawesome-svg-core"
import {faBars, faExclamationTriangle, faExternalLinkAlt, faInfoCircle, faUser} from "@fortawesome/free-solid-svg-icons"
import "./controllers"

library.add(faBars)
library.add(faExclamationTriangle)
library.add(faExternalLinkAlt)
library.add(faInfoCircle)
library.add(faUser)
dom.watch()

mrujs.start()
