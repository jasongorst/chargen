class KithainValidator < ActiveModel::Validator
  def validate(record)
    unless animal_seeming_present?(record)
      record.errors.add(:base, "animal_seeming must be present if kith is 'Pooka'")
    end

    unless kith_pooka?(record)
      record.errors.add(:base, "kith must be 'Pooka' if animal_seeming is present")
    end

    unless phylum_present?(record)
      record.errors.add(:base, "phylum must be present if kith is 'Inanimae'")
    end

    unless kith_inanimae?(record)
      record.errors.add(:base, "kith must be 'Inanimae' if phylum is present")
    end

    unless court_blank?(record)
      record.errors.add(:base, "court must not be present if kith is 'Inanimae'")
    end

    unless bannerhouse_truthbranch?(record)
      record.errors.add(:base, "kith must start with 'Sidhe' and house must be 'House Liam' if bannerhouse is 'Truthbranch'")
    end

    unless bannerhouse_cattecomb?(record)
      record.errors.add(:base, "kith must start with 'Sidhe' and house must be 'House Ailil' if bannerhouse is 'Cattecomb'")
    end
  end

  private

  def animal_seeming_present?(record)
    record.kith.name != 'Pooka' || record.animal_seeming.present?
  end

  def kith_pooka?(record)
    record.animal_seeming.blank? || record.kith.name == 'Pooka'
  end

  def phylum_present?(record)
    record.kith.name != 'Inanimae' || record.phylum.present?
  end

  def kith_inanimae?(record)
    record.phylum.blank? || record.kith.name == 'Inanimae'
  end

  def court_blank?(record)
    record.kith.name != 'Inanimae' || record.court.blank?
  end

  def bannerhouse_truthbranch?(record)
    record.bannerhouse.blank? || record.bannerhouse.name != 'Truthbranch' || (record.kith.name.start_with?('Sidhe') && record.house.name == 'House Liam')
  end

  def bannerhouse_cattecomb?(record)
    record.bannerhouse.blank? || record.bannerhouse.name != 'Cattecomb' || (record.kith.name.start_with?('Sidhe') && record.house.name == 'House Ailil')
  end
end
