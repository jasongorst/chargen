class RangedHash < Hash
  def [](key)
    self.each do |range, value|
      return value if range.include?(key)
    end
    nil
  end
end
