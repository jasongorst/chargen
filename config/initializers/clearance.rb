Clearance.configure do |config|
  config.allow_sign_up = false
  config.routes = false
  config.mailer_sender = "reply@evilpaws.org"
  config.rotate_csrf_on_sign_in = true
  config.redirect_url = "/admin"
  config.same_site = true
  config.secure_cookie = false
  config.signed_cookie = true
end
