module KithainHelper
  extend ActionView::Helpers::FormOptionsHelper
  extend ApplicationHelper::FormHelpers

  RANDOM_OPTION = options_for_select([['Random', '']])
  RANDOM_AND_NONE_OPTIONS = options_for_select([['Random', ''], ['None', 'none']])

  KITHAIN_ATTRIBUTES = {
    kith: {
      options: RANDOM_OPTION + model_options_with_rarity(:kiths),
      hidden?: false
    },
    seeming: {
      options: RANDOM_OPTION + model_options(Seeming),
      hidden?: false
    },
    animal_seeming: {
      options: RANDOM_OPTION + model_options_with_rarity(:animal_seemings),
      hidden?: true
    },
    phylum: {
      options: RANDOM_OPTION + model_options(Phylum),
      hidden?: true
    },
    house: {
      options: RANDOM_AND_NONE_OPTIONS + model_options_with_rarity(:houses),
      hidden?: false
    },
    court: {
      options: RANDOM_OPTION + model_options(Court),
      hidden?: false
    },
    bannerhouse: {
      options: RANDOM_AND_NONE_OPTIONS + model_options(Bannerhouse),
      hidden?: true
    },
    seelie_legacy: {
      options: RANDOM_OPTION + model_options(SeelieLegacy),
      hidden?: false
    },
    unseelie_legacy: {
      options: RANDOM_OPTION + model_options(UnseelieLegacy),
      hidden?: false
    },
    gender: {
      options: RANDOM_OPTION + model_options(Gender),
      hidden?: false
    }
  }

  def kithain_attributes
    Kithain.attribute_names.map(&:to_sym)
  end

  def kithain_attributes_with_properties
    KITHAIN_ATTRIBUTES
  end
end
