class CreateBannerhouses < ActiveRecord::Migration[6.1]
  def change
    create_table :bannerhouses do |t|
      t.string :name
      t.references :rarity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
