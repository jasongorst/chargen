class Character
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :seeming
  attribute :court
  attribute :unseelie_legacy
  attribute :gender

  validates :seeming, kind_of: true
  validates :court, kind_of: true, allow_nil: true
  validates :unseelie_legacy, kind_of: true
  validates :gender, kind_of: true
end
