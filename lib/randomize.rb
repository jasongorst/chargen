module Randomize
  module_function

  def random_attribute(model)
    model.offset(rand(model.count)).first
  end

  def random_attribute_with_rarity(model)
    rarity = Rarity.random
    model.where(rarity: rarity)
         .offset(rand(model.where(rarity: rarity).count))
         .first
  end
end
