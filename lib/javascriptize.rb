class String
  def javascriptize
    self.camelize :lower
  end
end

class Symbol
  def javascriptize
    self.to_s.javascriptize
  end
end
